<?php
	function form() {
		ob_start(); ?>
		<div class="form">
		    <a class="button tiny alert" href="#" id="showForm">show form</a>

		    <div id="ctl00_ctl13_UpdatePanelMiniCart">
		        <!-- minicart1 -->
		        <a href="/OrderSummary.aspx"><img border="0" height="15" src="http://www.shelterlogic.com/images/icn_cart.gif" style="margin-bottom: -3px;" width="19"> 10 Items in Cart $0.00</a> <!-- end minicart1 -->
		         | <a href="/ssl/ManageAccount/ManageAccount.aspx">My Account</a> | <a href="http://dealer.shelterlogicdirect.com">Dealer Login</a> | <a href="javascript:__doPostBack('ctl00$ctl13$LoginStatus$ctl02','')" id="ctl00_ctl13_LoginStatus">Sign In</a>
		    </div>

		    <form action="" id="form" method="post" name="form">
		        <select id="ctl00$ContentPlaceHolderBody$ctl00$ddlStyle" name="ctl00$ContentPlaceHolderBody$ctl00$ddlStyle" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentPlaceHolderBody$ctl00$ddlStyle\',\'\')', 0)">
		            <option value="0">
		                Select a Style
		            </option>

		            <option value="PK">
		                Peak Frame
		            </option>

		            <option value="RD">
		                Round Frame
		            </option>

		            <option value="BN">
		                Barn Frame
		            </option>
		        </select>

		        <table border="0" cellpadding="5" cellspacing="5" id="ctl00_ContentPlaceHolderBody_ctl00_tblOptions">
		            <tbody>
		                <tr>
		                    <td><select id="ctl00$ContentPlaceHolderBody$ctl00$ddlWidth" name="ctl00$ContentPlaceHolderBody$ctl00$ddlWidth" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentPlaceHolderBody$ctl00$ddlWidth\',\'\')', 0)">
		                        <option value="12">
		                            12'
		                        </option>

		                        <option value="15">
		                            15'
		                        </option>

		                        <option value="16">
		                            16'
		                        </option>

		                        <option value="20">
		                            20'
		                        </option>
		                    </select> <select id="ctl00$ContentPlaceHolderBody$ctl00$ddlHeight" name="ctl00$ContentPlaceHolderBody$ctl00$ddlHeight" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentPlaceHolderBody$ctl00$ddlHeight\',\'\')', 0)">
		                        <option value="09">
		                            9'
		                        </option>

		                        <option value="11">
		                            11'
		                        </option>

		                        <option value="14">
		                            14'
		                        </option>

		                        <option value="17">
		                            17'
		                        </option>
		                    </select> <select id="ctl00$ContentPlaceHolderBody$ctl00$ddlLength" name="ctl00$ContentPlaceHolderBody$ctl00$ddlLength" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentPlaceHolderBody$ctl00$ddlLength\',\'\')', 0)">
		                        <option value="20">
		                            20'
		                        </option>

		                        <option value="24">
		                            24'
		                        </option>

		                        <option value="28">
		                            28'
		                        </option>

		                        <option value="32">
		                            32'
		                        </option>

		                        <option value="36">
		                            36'
		                        </option>

		                        <option value="40">
		                            40'
		                        </option>

		                        <option value="44">
		                            44'
		                        </option>

		                        <option value="48">
		                            48'
		                        </option>

		                        <option value="52">
		                            52'
		                        </option>

		                        <option value="56">
		                            56'
		                        </option>

		                        <option value="60">
		                            60'
		                        </option>

		                        <option value="64">
		                            64'
		                        </option>

		                        <option value="68">
		                            68'
		                        </option>

		                        <option value="72">
		                            72'
		                        </option>

		                        <option value="76">
		                            76'
		                        </option>

		                        <option value="80">
		                            80'
		                        </option>

		                        <option value="84">
		                            84'
		                        </option>

		                        <option value="88">
		                            88'
		                        </option>

		                        <option value="92">
		                            92'
		                        </option>

		                        <option value="96">
		                            96'
		                        </option>

		                        <option value="100">
		                            100'
		                        </option>
		                    </select> <select id="ctl00$ContentPlaceHolderBody$ctl00$ddlMaterial" name="ctl00$ContentPlaceHolderBody$ctl00$ddlMaterial" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentPlaceHolderBody$ctl00$ddlMaterial\',\'\')', 0)">
		                        <option value="STD">
		                            Standard PE 9 oz
		                        </option>

		                        <option value="HDY">
		                            Heavy Duty PE 14.5 oz
		                        </option>

		                        <option value="PVC">
		                            Ultra Duty PVC 21.5 oz
		                        </option>
		                    </select> <select id="ctl00$ContentPlaceHolderBody$ctl00$ddlColor" name="ctl00$ContentPlaceHolderBody$ctl00$ddlColor" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentPlaceHolderBody$ctl00$ddlColor\',\'\')', 0)">
		                        <option value="GY">
		                            Gray
		                        </option>

		                        <option value="GN">
		                            Green
		                        </option>

		                        <option value="TN">
		                            Tan
		                        </option>

		                        <option value="WE">
		                            White
		                        </option>

		                        <option value="CR">
		                            Clear
		                        </option>

		                        <option value="BN">
		                            Brown
		                        </option>
		                    </select></td>
		                </tr>
		            </tbody>
		        </table>
		    </form>

		    <div id="ctl00_ContentPlaceHolderBody_ctl00_pnlDetails">
		        <table border="0" cellpadding="5" cellspacing="5" style="padding-left: -3px;" width="221">
		            <tbody>
		                <tr>
		                    <td>Style</td>

		                    <td align="right" style="font-weight: bold"><span id="ctl00_ContentPlaceHolderBody_ctl00_lblStyle">Round Frame</span></td>
		                </tr>

		                <tr>
		                    <td>Width</td>

		                    <td align="right"><strong><span id="ctl00_ContentPlaceHolderBody_ctl00_lblWidth">14'</span></strong></td>
		                </tr>

		                <tr>
		                    <td>Height</td>

		                    <td align="right"><strong><span id="ctl00_ContentPlaceHolderBody_ctl00_lblHeight">07'</span></strong></td>
		                </tr>

		                <tr>
		                    <td>Length</td>

		                    <td align="right"><strong><span id="ctl00_ContentPlaceHolderBody_ctl00_lblLength">20'</span></strong></td>
		                </tr>

		                <tr>
		                    <td>Fabric</td>

		                    <td align="right"><strong><span id="ctl00_ContentPlaceHolderBody_ctl00_lblMaterial">Standard PE 9 oz</span></strong></td>
		                </tr>

		                <tr>
		                    <td>Color</td>

		                    <td align="right"><strong><span id="ctl00_ContentPlaceHolderBody_ctl00_lblColor">Gray</span></strong></td>
		                </tr>

		                <tr>
		                    <td align="right" class="yellow" colspan="3">Building Total: <b><span id="ctl00_ContentPlaceHolderBody_ctl00_lblCost">$1,359.36</span></b></td>
		                </tr>
		            </tbody>
		        </table>

		        <div class="yellow" style="border-top: 1px solid #404040; margin-top: 10px; padding-bottom: 10px; font-size: 12px;">
		            <br>
		            <b>Accessories</b>
		        </div>

		        <table border="0" cellpadding="5" cellspacing="0" id="tblAccessoriesTotal" style="padding-left: -3px;" width="221">
		            <tbody></tbody>
		        </table>

		        <table border="0" cellpadding="5" cellspacing="0" style="padding-left: -3px;" width="221">
		            <tbody>
		                <tr>
		                    <td align="right" class="yellow" colspan="2" style="padding-top: 10px;">Accessories Total: <b><span id="ctl00_ContentPlaceHolderBody_ctl00_lblAccessoriesTotal">$0.00</span></b></td>
		                </tr>
		            </tbody>
		        </table>

		        <div class="total" style="text-align: right">
		            <span style="font-size: 14px; font-weight: bold;">Grand Total: <span id="ctl00_ContentPlaceHolderBody_ctl00_lblGrandTotal">$1,359.36</span>*</span><br>
		            *Before taxes and shipping costs
		        </div>

		        <div style="text-align: right">
		            <a class="thickbox" href="#TB_inline?height=650&amp;width=575&amp;inlineId=TermsAndConditionsPopup&amp;modal=true"><img alt="Add To Cart" height="19" src="http://shelterlogic.com/images/btn_addtocart.png" style="background: transparent; border:0;" width="106"></a>
		        </div>
		    </div>
		</div><!-- END form for local development -->
          <?php ob_end_flush();
	} ?>