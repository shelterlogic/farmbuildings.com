<div class="contact" id="contactModal">
	<div id="s_logo" class="logo dup align-center hide-for-small-only">
		<span class="title main">
			<span class="st-s"></span>
			<span class="st-h"></span>
			<span class="st-e"></span>
			<span class="st-l"></span>
			<span class="st-t1"></span>
			<span class="st-e"></span>
			<span class="st-r"></span>
			<span class="st-t yellow"></span>
			<span class="st-e yellow"></span>
			<span class="st-c yellow"></span>
			<span class="st-h no-break yellow"></span>
		</span>
	</div>
	<div class="row">
	  <div class="small-12 medium-10 large-8 small-centered columns">
	    <h2>Contact us.</h2>
	    <p class="justify contactSmall">Whether you have a question about our products or just want to drop us a line, we would love to hear from you.</p>
	  </div>
	</div>
	<div class="row">
	  <div class="small-12 medium-10 large-8 small-centered columns">
	    <div class="small-12 meduim-7 large-7 columns">
	      <form>
	        <div class="border-right">
	            <label>Name
                  <input type="text" placeholder="Name" />
                </label>
            
                <label>Email Address
                  <input type="text" placeholder="youremail@email.com" />
                </label>
            
                <label>Phone Number
                  <input type="text" placeholder="(000) 000 - 0000" />
                </label>
            
                <label>Message
                  <textarea placeholder="Your Message Here."></textarea>
                </label>
	        </div>
	        <input type="button" class="button active" value="CONTACT US" />
	      </form>
	    </div>
	    <div class="small-12 meduim-5 large-5 columns" id="contactTable">
	    	<strong>SHELTERTECH SALES</strong>
	    	<a href="mailto:sheltertech@shelterlogic.com">sheltertech@shelterlogic.com</a>
			<div id="contactBlock" itemscope itemtype="http://schema.org/LocalBusiness">
			  <span itemprop="name">ShelterLogic Corp</span>
			  <div class="address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
			    <span class="block" itemprop="streetAddress">150 Callender Road</span>
			    <span itemprop="addressLocality">Watertown</span>,
			    <span itemprop="addressRegion">CT</span> 
			    <span itemprop="postalCode">06795</span>
			  </div>
			  <span id="description" itemprop="description"> <a href="https://www.facebook.com/ShelterLogic1" target="_blank"><i class="fa fa-facebook-square"></i></a> <a href="https://twitter.com/ShelterLogic" target="_blank"><i class="fa fa-twitter"></i></a> <span>Speak with a live representative</span></span>
			  <i class="fa fa-mobile"></i> <span class="phone" itemprop="telephone">1-800-932-9334</span>
			</div>

	    </div>
	  </div>
	</div>
</div>
<a class="close-reveal-modal">&#215;</a>