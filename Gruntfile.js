'use strict';

module.exports = function(grunt) {
  var mozjpeg = require('imagemin-mozjpeg');
  grunt.initConfig({
    imagemin: {                          // Task
      // static: {                          // Target
      //   options: {                       // Target options
      //     optimizationLevel: 3,
      //     use: [mozjpeg()]
      //   },
      //   files: {                         // Dictionary of files
      //     'dist/img.png': 'src/img.png', // 'destination': 'source'
      //     'dist/img.jpg': 'src/img.jpg',
      //     'dist/img.gif': 'src/img.gif'
      //   }
      // },
      dynamic: {                         // Another target
        options: {                       // Target options
          optimizationLevel: 3,
          use: [mozjpeg()]
        },
        files: [{
          expand: true,                  // Enable dynamic expansion
          cwd: 'images/src/',                   // Src matches are relative to this path
          src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
          dest: 'images/dist/'                  // Destination path prefix
        }]
      }
    },
    //pkg: grunt.file.readJSON('package.json'),
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        './js/uncompiled/*.js',
        './js/uncompiled/plugins/*.js',
      ]
    },
    sass: {
      options: {
        includePaths: ['bower_components/foundation/scss']
      },
      dist: {
        options: {
          outputStyle: 'uncompressed'
        },
        files: {
          'css/app.css': 'scss/app.scss'
        }        
      }
    },
    uglify: {
      dist: {
        options: {
          sourceMap: true,
          sourceMapIncludeSources: true,
          sourceMapName: 'js/app.sourceMap.map'
        },
        files: {
          // COMPONENTS
          'js/components.min.js': [
            'bower_components/modernizr/modernizr.js',
            'bower_components/jquery/dist/jquery.js',
            'bower_components/jquery-placeholder/jquery.placeholder.js',
            'bower_components/jquery.cookie/jquery.cookie.js',
            'bower_components/respond/src/respond.js',
            'bower_components/selectivizr/selectivizr.js',
            'bower_components/foundation/js/foundation.js',
            'bower_components/FitText/jquery.fittext.js',
            //'bower_components/SlabText/js/jquery.slabtext.js',
            //'bower_components/jquery-hashchange/jquery.ba-hashchange.js',
            //'bower_components/jquery.scrollTo/jquery.scrollTo.js',
            'bower_components/source-web-files/js/custom/detect.browser.js',
            //'bower_components/source-web-files/js/custom/mobile.onchange.js',
            'bower_components/source-web-files/js/custom/center.center.js',
            'bower_components/source-web-files/js/custom/make.square.js',
            'bower_components/source-web-files/js/custom/make.windowHeight.js',
            'bower_components/source-web-files/js/custom/smart.resize.js'
          ],
          // MAIN SCRIPTS...
          'js/app.min.js': [
            'js/plugins/*.js',
            'js/uncompiled/doors.load.object.js',
            'js/uncompiled/app.js',
          ],
          'js/pace.min.js': [
            'bower_components/pace/pace.js',
            // error pace is not defined, comment out below
            //'js/uncompiled/pace.config.js',
          ],
        }
      }
    },
    phpunit: {
      //...
      options: {}
    },
    watch: {
      sass: {
        options: {
          livereload: true
        },
        files: 'scss/**/*.scss',
        tasks: ['sass']
      },
      js: {
        options: {
          livereload: true
        },
        files: '<%= jshint.all %>',
        tasks: ['uglify'],
      },
    }
  });
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sass');
  

  // Register tasks
  grunt.registerTask('default', [
    'sass',
    'uglify',
    'imagemin',
  ]);
  grunt.registerTask('dev', [
    'watch'
  ]);
}