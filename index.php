<?php include 'lib/functions.php'; ?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Foundation</title>
    <link rel="stylesheet" href="css/app.css" />
    <script src="js/pace.min.js" type="text/javascript"></script>
  </head>
  <body>   
    <div class="off-canvas-wrap docs-wrap" data-offcanvas>
      <div class="inner-wrap winH">
        <i id="loader" class="fa fa-circle-o-notch fa-spin"></i>
        <nav class="tab-bar">
          <section class="left-small">
            <a class="left-off-canvas-toggle menu-icon"><span></span></a>
          </section>
          <section class="right tab-bar-section">
            <div class="row">
              <div class="medium-2 show-for-medium-up columns">&nbsp;</div>
              <div class="small-8 medium-8 columns nomar nopadd">
                <div id="mainLogo" class="logo align-center main">
                  <span class="title main">
                    <span class="st-s"></span>
                    <span class="st-h"></span>
                    <span class="st-e"></span>
                    <span class="st-l"></span>
                    <span class="st-t1"></span>
                    <span class="st-e"></span>
                    <span class="st-r"></span>
                    <span class="st-t yellow"></span>
                    <span class="st-e yellow"></span>
                    <span class="st-c yellow"></span>
                    <span class="st-h no-break yellow"></span>
                  </span>
                </div>
              </div>
              <div class="small-4 medium-2 columns align-right nomar nopadd">
                <a href="http://www.shelterlogic.com/OrderSummary.aspx" target="_blank"><i class="fa fa-shopping-cart"><span>0</span></i></a>
                <a href="#contact" class="modalAjax" data-reveal-id="ajaxModal" data-reveal-ajax="true">Help</a>
              </div>
            </div>
            
            
          </section>

        </nav>
        <!-- Off Canvas Menu -->
        <aside class="left-off-canvas-menu">
            <!-- whatever you want goes here -->
            <ul class="off-canvas-list">
              <li><label>SP Series</label></li>
              <li><a href="#features" class="modalAjax" data-reveal-id="ajaxModal" data-reveal-ajax="true">Features</a></li>
              <li><a href="#warranty" class="modalAjax" data-reveal-id="ajaxModal" data-reveal-ajax="true">Warranty</a></li>
              <li><a href="#contact" class="modalAjax" data-reveal-id="ajaxModal" data-reveal-ajax="true">Contact Us</a></li>
              <li><a href="#quote" data-reveal-id="quoteModal">Quote Modal</a></li>
              <li><label>ShelterLogic</label><modalAjax/li>
              <li><a href="#terms" class="modalAjax" data-reveal-id="ajaxModal" data-reveal-ajax="true">Terms &amp; Conditions</a></li>
              <li><a href="#privacy" class="modalAjax" data-reveal-id="ajaxModal" data-reveal-ajax="true">Privacy Policy</a></li>
              <li><a href="#startOver" class="UI startOver" data-func="startOver">Start Over</a></li>
            </ul>
        </aside>
        <section class="main-section">
          <?php form(); ?>
          <div class="row" id="mainUIElements">
            <div class="large-12 columns winH">
              
              <!-- add elements div here -->

                <div class="elements leftSide" addClass="centerVertical to vertically align">
                  <div class="UITop">
                    <div class="mobileColumn">
                      <span id="productTitle">SP&nbsp;SERIES</span>
                      <span id="shortDesc">GALVANIZED&nbsp;BUILDINGS</span>
                    </div>
                    <div class="mobileColumn">
                      <span class="calcPrice">$0.00</span>
                    </div>
                    <div class="mobileColumn">
                      <button id="addToCart" class="UI button nomar-bottom expand disabled">ADD TO CART</button>
                    </div>
                  </div>
                  <div class="UIBottom">
                    <div class="UIBuilderContainer highlight">
                      <div id="quickNav" class="row"></div>
                      <div class="UIBuilderNav clearfix">
                        <span class="left"><i class="UI fa fa-chevron-left prev" data-func="toggleQuickNav" ></i></span>
                        <span class="right"><i class="UI fa fa-chevron-right next" data-func="whichChoice" ></i></span>
                      </div>
                      <ul id="UIList">
                        <li class="UISection clear" id="chooseStyle">
                          <span class="UI UISectionTitle" data-func="toggleQuickNav">Choose a frame <i class="showInfo UI fa fa-info-circle hide-for-large-up" data-func="getInfo" data-infoid="infoStyle"></i></span>
                          <div class="buttonContainer" id="build_Style"></div>
                          <div class="spacing align-right show-for-large-up">
                            <i class="showInfo UI fa fa-info-circle" data-func="getInfo" data-infoid="infoStyle"></i>
                          </div>
                        </li>
                        <li class="UISection" id="chooseWidth">
                          <span class="UI UISectionTitle" data-func="toggleQuickNav">Choose a width <i class="showInfo UI fa fa-info-circle hide-for-large-up" data-func="getInfo" data-infoid="infoWidth"></i></span>
                          <select name="builderWidth" id="builderWidth" class="UI UI-Option" data-func="setAttr" data-type="Width" ></select>
                          <div class="spacing align-right show-for-large-up">
                            <i class="showInfo UI fa fa-info-circle" data-func="getInfo" data-infoid="infoWidth"></i>
                          </div>
                        </li>
                        <li class="UISection" id="chooseHeight">
                          <span class="UI UISectionTitle" data-func="toggleQuickNav">Choose a height <i class="showInfo UI fa fa-info-circle hide-for-large-up" data-func="getInfo" data-infoid="infoHeight"></i></span>
                          <select name="builderHeight" id="builderHeight" class="UI UI-Option" data-func="setAttr" data-type="Height" ></select>
                          <div class="spacing align-right show-for-large-up">
                            <i class="showInfo UI fa fa-info-circle" data-func="getInfo" data-infoid="infoHeight"></i>
                          </div>
                        </li>
                        <li class="UISection" id="chooseLength">
                          <span class="UI UISectionTitle" data-func="toggleQuickNav">Choose a Length <i class="showInfo UI fa fa-info-circle hide-for-large-up" data-func="getInfo" data-infoid="infoLength"></i></span>
                          <select name="builderLength" id="builderLength" class="UI UI-Option" data-func="setAttr" data-type="Length" ></select>
                          <div class="spacing align-right show-for-large-up">
                            <i class="showInfo UI fa fa-info-circle" data-func="getInfo" data-infoid="infoLength"></i>
                          </div>
                        </li>
                        <li class="UISection" id="chooseMaterial">
                          <span class="UI UISectionTitle" data-func="toggleQuickNav">Choose a fabric <i class="showInfo UI fa fa-info-circle hide-for-large-up" data-func="getInfo" data-infoid="infoMaterial"></i></span>
                          <div class="buttonContainer" id="build_Material"></div>
                          <div class="spacing align-right show-for-large-up">
                            <i class="showInfo UI fa fa-info-circle" data-func="getInfo" data-infoid="infoMaterial"></i>
                          </div>
                        </li>
                        <li class="UISection" id="chooseColor">
                          <span class="UI UISectionTitle" data-func="toggleQuickNav">Choose a color <i class="showInfo UI fa fa-info-circle hide-for-large-up" data-func="getInfo" data-infoid="infoColor"></i></span>
                          <div class="buttonContainer" id="build_Color">
                            <button class="UI UI-Option button expand box-shadow textCenter makeSquare" data-func="setAttr" data-type="Color" data-value="GY"> Gray <i class="fa fa-check centerVertical centerHorizontal"></i></button>
                            <button class="UI UI-Option button expand box-shadow textCenter makeSquare disable" data-func="setAttr" data-type="Color" data-value="GN"> Green <i class="fa fa-check centerVertical centerHorizontal"></i></button>
                            <button class="UI UI-Option button expand box-shadow textCenter makeSquare disable" data-func="setAttr" data-type="Color" data-value="TN"> Tan <i class="fa fa-check centerVertical centerHorizontal"></i></button>
                            <button class="UI UI-Option button expand box-shadow textCenter makeSquare disable" data-func="setAttr" data-type="Color" data-value="WE"> White <i class="fa fa-check centerVertical centerHorizontal"></i></button>
                            <button class="UI UI-Option button expand box-shadow textCenter makeSquare disable" data-func="setAttr" data-type="Color" data-value="CR"> Clear <i class="fa fa-check centerVertical centerHorizontal"></i></button>
                            <button class="UI UI-Option button expand box-shadow textCenter makeSquare disable" data-func="setAttr" data-type="Color" data-value="BN"> Brown <i class="fa fa-check centerVertical centerHorizontal"></i></button>
                          </div>
                          <div class="spacing align-right show-for-large-up">
                            <i class="showInfo UI fa fa-info-circle" data-func="getInfo" data-infoid="infoColor"></i>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div id="infoMain" class="show-for-large-up">
                      <div class="fa fa-stop info infoStyle infoBuilderBox right">
                        <i class="UI fa fa-plus UIInfoClose" data-func="closeInfo"></i>
                        <div class="scroll">
                          <h3>Frame Style</h3>
                          <span class="name yellow">PEAK</span>
                          <p>The Peak style frame originates from the classic house gable and is designed for ease of installation and storage shelter versatility.</p>
                          <span class="name yellow">ROUND</span>
                          <p>Our most popular roof style, the Round design is the most effective at shedding the elements. The Round Series includes the greatest variety of width options of the SP Series, making it extremely adaptable.</p>
                          <span class="name yellow">BARN</span>
                          <p>A ShelterLogic innovation, the Barn style features an overall increase in cubic storage space and more usable headroom than other frame styles.</p>
                        </div>
                      </div>
                      <div class="fa fa-stop info infoWidth infoBuilderBox right">
                        <i class="UI fa fa-plus UIInfoClose" data-func="closeInfo"></i>
                        <div class="scroll"></div>
                      </div>
                      <div class="fa fa-stop info infoHeight infoBuilderBox right">
                        <i class="UI fa fa-plus UIInfoClose" data-func="closeInfo"></i>
                        <div class="scroll"></div>
                      </div>
                      <div class="fa fa-stop info infoLength infoBuilderBox right">
                        <i class="UI fa fa-plus UIInfoClose" data-func="closeInfo"></i>
                        <div class="scroll"></div>
                      </div>
                      <div class="fa fa-stop info infoMaterial infoBuilderBox right">
                        <i class="UI fa fa-plus UIInfoClose" data-func="closeInfo"></i>
                        <div class="scroll">
                          <table role="grid" class="infoWarranty" summary="Fabric Warranty Summary" cellspacing="0">
                            <tr>
                              <th>Fabric</th>
                              <th>Cover</th>
                              <th>Panels</th>
                              <th>Frame</th>
                            </tr>
                            <tr>
                              <td>
                                <strong>Standard PE <br>9 oz.</strong>
                              </td>
                              <td>1/yr</td>
                              <td>1/yr</td>
                              <td>3/yr<sup>*</sup></td>
                            </tr>
                            <tr>
                              <td>
                                <strong>Heavy Duty PE <br>14.5 oz.</strong>
                              </td>
                              <td>10/yr<sup>*</sup></td>
                              <td>3/yr<sup>*</sup></td>
                              <td>3/yr<sup>*</sup></td>
                            </tr>
                            <tr>
                              <td>
                                <strong>Ultra Duty PVC<br>21.5 oz.</strong>
                              </td>
                              <td>15/yr<sup>*</sup></td>
                              <td>3/yr<sup>*</sup></td>
                              <td>3/yr<sup>*</sup></td>
                            </tr>
                          </table>
                          <sup>* Prorated</sup>
                        </div>
                      </div>
                      <div class="fa fa-stop info infoColor infoBuilderBox right">
                        <i class="UI fa fa-plus UIInfoClose" data-func="closeInfo"></i>
                        <div class="scroll">
                          <h3>Colors</h3>
                          <p>All colors of the same fabric grade will last and preform the same. Choose your color based on your visual preference.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <a href="#startOver" class="UI startOver show-for-large-up" data-func="startOver">Start Over</a>
                  <div id="mobileInfoMain" class="hide-for-large-up"></div>
                </div>
                <div class="elements rightSide">
                  <div>
                    <dl id="mainAccordion" class="accordion show-for-large-up" data-accordion data-options="multi_expand:true;toggleable: true">
                      <dd class="accordion-navigation UITop nopadd" id="detailsAccordion">
                        <a href="#panel1" class="panelnav">SPECIFICATIONS <i class="fa fa-chevron-down right"></i></a>
                        <div id="panel1" class="content active">
                          <!-- <i class="fa fa-arrow-up hide-for-large-up UI" data-func="closeAccordion"></i> -->
                          <div id="StyleSelected" class="spacing"><span class="cap">Frame</span> <span class="attribute right">-</span></div>
                          <div id="WidthSelected" class="spacing"><span class="cap">Width</span> <span class="attribute right">-</span></div>
                          <div id="HeightSelected" class="spacing"><span class="cap">Height</span> <span class="attribute right">-</span></div>
                          <div id="LengthSelected" class="spacing"><span class="cap">Length</span> <span class="attribute right">-</span></div>
                          <div id="MaterialSelected" class="spacing"><span class="cap">Fabric</span> <span class="attribute right">-</span></div>
                          <div id="ColorSelected" class="spacing"><span class="cap">color</span> <span class="attribute right">-</span></div>
                        </div>
                      </dd>
                      <dd class="accordion-navigation UITop nopadd">
                        <a href="#panel2" class="panelnav">DOOR OPENING <i class="fa fa-chevron-down right"></i></a>
                        <div id="panel2" class="content active">
                          <!-- <i class="fa fa-arrow-up hide-for-large-up UI" data-func="closeAccordion"></i> -->
                          <div id="door_width" class="spacing"><span class="cap">Width</span> <span class="attribute right">-</span></div>
                          <div id="door_height" class="spacing"><span class="cap">Height</span> <span class="attribute right">-</span></div>
                          <div id="door_center_height" class="spacing"><span class="cap">Center Height</span> <span class="attribute right">N/A</span></div>
                          <div id="diagram" class="diagram spacing show-for-large-up hide"><a href="#" data-reveal-id="diagramModal">See Diagram</a></div>
                        </div>
                      </dd>
                      <dd class="accordion-navigation UIBottom UITop nopadd clearfix">
                        <a href="#panel3" class="panelnav"><span class="hide-for-large-up">WIND&nbsp;&amp;&nbsp;SNOW <i class="fa fa-chevron-down right"></i></span><span class="show-for-large-up">WIND&nbsp;&amp;&nbsp;SNOW&nbsp;RATING <i class="fa fa-chevron-down right"></i></span> </a>
                        <div id="panel3" class="content active">
                          <!-- <i class="fa fa-arrow-up hide-for-large-up UI" data-func="closeAccordion"></i> -->
                          <div id="wind_load" class="spacing"><span class="cap">Wind Speed</span> <span class="attribute right">N/A</span></div>
                          <div id="snow_load" class="spacing"><span class="cap">Snow Load</span> <span class="attribute right">N/A</span></div>
                          <div class="spacing">
                            <i class="showInfo UI fa fa-info-circle" data-func="getInfo" data-infoid="infoWind"></i>
                          </div>
                        </div>
                        <div class="fa fa-stop infoWind info infoBuilderBox show-for-large-up left">
                          <i class="UI fa fa-plus UIInfoClose rightInfo" data-func="closeInfo"></i>
                          <div class="scroll">
                            <img src="images/src/sp_wind_snow.png" class="windAndSnow left">
                            <!-- <h3>Wind &amp; Snow</h3> -->
                            <p>Wind and snow loads given are for reference only and assume a securely anchored frame according to local building codes and ordinances. These have been calculated using combined snow and wind load provisions in accordance with ASCE 7-05 based on an unoccupied, fully enclosed building. Local code requirements vary by location; Conformance to local codes is the responsibility of the purchaser.</p>
                          </div>
                        </div>
                      </dd>
                    </dl>
                    <dl id="mobileAccordion" class="accordion hide-for-large-up" data-accordion data-options="multi_expand:false;toggleable: true"></dl>
                    <div class="infoWind info infoBuilderBox hide-for-large-up left">
                      <i class="UI fa fa-plus UIInfoClose" data-func="closeInfo"></i>
                      <h3>test</h3>
                      <p>Lorem ipsum dolor sit amet.</p>
                    </div>
                  </div>
                </div>

              <!-- end elements -->
              
            </div>
          </div>
          <div id="footer">
            <img src="images/src/logo.png" alt="ShelterLogic">
            <span class="copyright">&copy; 2014 ShelterLogic® Corp.  All Rights Reserved.</span>
            <span class="termsPrivacy"><a href="#terms" class="modalAjax" data-reveal-id="ajaxModal" data-reveal-ajax="true">Terms &amp; Conditions</a><a href="#privacy" class="modalAjax" data-reveal-id="ajaxModal" data-reveal-ajax="true">Privacy Policy</a></span>
          </div>
        </section>

        <!-- close the off-canvas menu -->
        <a class="exit-off-canvas"></a>
      </div>
    </div>
    <div id="building" class="winHW">
      <img src="http://s7d2.scene7.com/is/image/ShelterLogic/blank_logo?fmt=png-alpha" class="centerCenter" alt="">
    </div>
    <div class="preload"></div>
    <div id="startModal" class="reveal-modal full" data-reveal>
      <div id="s_logo" class="logo dup align-center hide-for-small-only"></div>
      <div class="row">
        <div class="small-12 medium-10 large-8 small-centered columns">
          <h2 class="align-center">Build your own shelter</h2>
          <p class="align-center hide-for-small-only"><img src="images/src/made-in.png" alt="made in usa" class="left"> 
            <img src="images/src/sp_wind_snow.png" alt="wind and snow rated" class="right">Protect what you value most with our USA-made SP Series shelters.  Only certified galvanized steel is used to ensure you the quality, durable shelter you expect from ShelterTech.</p>
          <h4 class="align-center">CHOOSE A FRAME STYLE TO START BUILDING YOUR SHELTER</h4>
        </div>
      </div>
      <div class="row" id="startStyle">
        <div class="show-for-large-up">
          <div class="styles style peak">
            <img class="" id="start_peak" alt="Peak style frame" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjAAAAGQAQMAAACKy8hfAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAADJJREFUeNrtwYEAAAAAw6D5Ux/gClUBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAG27wAAHjW3aTAAAAAElFTkSuQmCC"/>
            <div class="styleText">
              <span class="name">PEAK</span>
              <p>The Peak style frame originates from the classic house gable and is designed for ease of installation and storage shelter versatility.</p>
              <button class="UI button nomar-bottom" data-func="setStyle" data-type="Style" data-value="PK">Choose Peak</button>
            </div>
          </div>
          <div class="styles style round">
            <img class="" id="start_round" alt="Round style fram" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjAAAAGQAQMAAACKy8hfAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAADJJREFUeNrtwYEAAAAAw6D5Ux/gClUBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAG27wAAHjW3aTAAAAAElFTkSuQmCC" />
            <div class="styleText">
              <span class="name">ROUND</span>
              <p>Our most popular roof style, the Round design is the most effective at shedding the elements. The Round Series includes the greatest variety of width options of the SP Series, making it extremely adaptable.</p>
              <button class="UI button nomar-bottom" data-func="setStyle" data-type="Style" data-value="RD">Choose Round</button>
            </div>
          </div>
          <div class="styles style barn">
            <img class="" id="start_barn" alt="Barn style frame" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjAAAAGQAQMAAACKy8hfAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAADJJREFUeNrtwYEAAAAAw6D5Ux/gClUBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAG27wAAHjW3aTAAAAAElFTkSuQmCC" />
            <div class="styleText">
              <span class="name">BARN</span>
              <p>A ShelterLogic innovation, the Barn style features an overall increase in cubic storage space and more usable headroom than other frame styles.</p>
              <button class="UI button nomar-bottom" data-func="setStyle" data-type="Style" data-value="BN">Choose Barn</button>
            </div>
          </div>
        </div>
        
        <div class="buttonContainer hide-for-large-up" id="build_Style"></div>
      </div>
      <div class="row">
        <div class="small-12 medium-10 large-8 small-centered columns">
          <p class="made-in clearfix">Need help? Call 1-800-932-9344 or email us at sheltertech@shelterlogic.com.</p>
        </div>
      </div>

    </div>
    <div id="cookieModal" class="reveal-modal" data-reveal>
      <div id="c_Logo" class="logo dup align-center hide-for-small-only"></div>
      <p class="lead">Sorry! Cookies must be enabled...</p>
    </div>
    <div id="diagramModal" class="reveal-modal full" data-reveal>
      <div id="d_Logo" class="logo dup align-center hide-for-small-only"></div>
      <a class="close-reveal-modal">&#215;</a>
      <img src="" alt="" id="theDiagram" />
    </div>
    <div id="landscapeModal" class="reveal-modal" data-reveal>
        <h2>Your screen is too small!</h2>
        <p class="lead">You should switch to portrait, then we will let you continue.</p>
    </div>
    <div id="ajaxModal" class="reveal-modal full" data-reveal></div>
    <div id="quoteModal" class="reveal-modal full" data-reveal>
      <div id="s_logo" class="logo dup align-center hide-for-small-only"></div>
      <div class="row">
        <div class="small-12 medium-10 large-8 small-centered columns">
          <h2>Contact us for a shipping quote.</h2>
          <p class="justify contactSmall">Because the building you configured <strong>exceeds 5,000 pounds</strong>, we will need to provide you with a custom shipping quote.  Please complete the form below and one of our customer service representatives will be in touch with you shortly.</p>
        </div>
      </div>
      <div class="row">
        <div class="small-12 medium-10 large-8 small-centered columns">
          <div class="small-12 meduim-7 large-7 columns">
            <form>
              <div class="border-right">
                  <label>First Name
                    <input type="text" placeholder="First Name" />
                  </label>
              
                  <label>Last Name
                    <input type="text" placeholder="Last Name" />
                  </label>
              
                  <label>Email Address
                    <input type="text" placeholder="youremail@email.com" />
                  </label>
              
                  <label>Phone Number
                    <input type="text" placeholder="(000) 000 - 0000" />
                  </label>
              
                  <label>Zip Code
                    <input type="text" placeholder="00000" />
                  </label>
              </div>
              <input type="button" class="button active" value="GET A SHIPPING QUOTE" />
            </form>
          </div>
          <div class="small-12 meduim-5 large-5 columns" id="infoTable">
             <dl id="quoteAccordion" class="accordion show-for-large-up" data-accordion data-options="multi_expand:false;toggleable: false">
                <dd class="accordion-navigation UITop nopadd" id="detailsAccordion">
                  <a href="#panel1" class="panelnav">Order Summary <i class="fa fa-chevron-down right"></i></a>
                  <div id="panel1" class="content active">
                    <!-- <i class="fa fa-arrow-up hide-for-large-up UI" data-func="closeAccordion"></i> -->
                    <div id="StyleSelected" class="spacing"><span class="cap">Frame</span> <span class="attribute right">-</span></div>
                    <div id="WidthSelected" class="spacing"><span class="cap">Width</span> <span class="attribute right">-</span></div>
                    <div id="HeightSelected" class="spacing"><span class="cap">Height</span> <span class="attribute right">-</span></div>
                    <div id="LengthSelected" class="spacing"><span class="cap">Length</span> <span class="attribute right">-</span></div>
                    <div id="MaterialSelected" class="spacing"><span class="cap">Fabric</span> <span class="attribute right">-</span></div>
                    <div id="ColorSelected" class="spacing"><span class="cap">color</span> <span class="attribute right">-</span></div>
                  </div>
                </dd>
              </dl>
              <div class="align-right totals"><span class="subtotal">Subtotal</span><span class="calcPrice">$0.00</span></div>
              <div class="small-2 columns">
                OR
              </div>
              <div class="small-10 columns">
                <span>Speak with a live representative</span><br>
                <i class="fa fa-mobile"></i> <span class="phone" itemprop="telephone">1-800-932-9334</span>
              </div>
              
          </div>
        </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
    </div>
    <!-- modal example 
      <div id="startModal" class="reveal-modal" data-reveal>
        <h2>Awesome. I have it.</h2>
        <p class="lead">Your couch.  It is mine.</p>
        <p>Im a cool paragraph that lives inside of an even cooler modal. Wins</p>
        <a class="close-reveal-modal">&#215;</a>
      </div>
    -->
    <script src="js/components.min.js"></script>
    <script src="js/app.min.js"></script>
  </body>
</html>
