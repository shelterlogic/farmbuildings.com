<?php

    // Include the DirectoryLister class
    require_once('resources/DirectoryLister.php');

    // Include the parsedown library to read .md files
    require_once('resources/parsedown/Parsedown.php');

    // Initialize the DirectoryLister object
    $lister = new DirectoryLister();

    // Initialize Parsedown
    global $Parsedown;
    $Parsedown = new Parsedown();
    // Parse down can be called on any page now by getting the contents of a .md file and outputting it to the screen i.e.
    // echo $Parsedown->text('Hello _Parsedown_!');
    
    // Set a global to be reset later
    global $readme; 
    function readme($filePath) {
        global $Parsedown;
        $content = file_get_contents($filePath, true);
        $readmeHtml = $Parsedown->text($content);
        return $readmeHtml;
    }

    // Return file hash
    if (isset($_GET['hash'])) {

        // Get file hash array and JSON encode it
        $hashes = $lister->getFileHash($_GET['hash']);
        $data   = json_encode($hashes);

        // Return the data
        die($data);

    }

    // Initialize the directory array
    if (isset($_GET['dir'])) {
        $dirArray = $lister->listDirectory($_GET['dir']);
    } else {
        $dirArray = $lister->listDirectory('.');
    }

    // Define theme path
    if (!defined('THEMEPATH')) {
        define('THEMEPATH', $lister->getThemePath());
    }

    // Set path to theme index
    $themeIndex = $lister->getThemePath(true) . '/index.php';

    // Initialize the theme
    if (file_exists($themeIndex)) {
        include($themeIndex);
    } else {
        die('ERROR: Failed to initialize theme');
    }
