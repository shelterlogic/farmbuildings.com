<!DOCTYPE html>

<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Foundation | Welcome</title>
    <link href="/foundation/css/foundation.css" rel="stylesheet">
    <link href="/scss/mainpage/main.style.css" rel="stylesheet">
    <script src="/foundation/js/vendor/modernizr.js"></script>
</head>

<body>
    <!-- Header and Nav -->
    <div class="tertiary-nav">
        <div class="row top-bar">
            <div class="small-12 columns">
                <div class="logo left"><a href="/"><img src="http://www.shelterlogic.com/images/logo.gif" alt=""></a></div>
                <ul class="inline-list right">
                    <!-- <li><a class="menu" href="#"><span>Menu</span></a></li> -->
                </ul>
            </div>
        </div>
    </div>
    <!-- End Header and Nav -->