    <div class="row results">
        <div class="large-12 columns">

            <div class="search-results">
                <div class="row" data-bind="foreach: asset">
                    <div class="large-2 columns" data-bind="attr: { 'data-type': type }">
                        <a data-bind="attr: { href: source, title: title }"><img alt="shot cover" class=" thumbnail" data-bind="attr: { src: thumb, alt: title}" src=""></a>
                    </div>

                    <div class="large-10 columns">
                        <div class="row">
                            <div class=" large-9 columns">
                                <h5><a data-bind="attr: { href: source }, text: title" href="#" ></a></h5>
                            </div>

                            <div class=" large-3 columns">
                                <a class="button expand medium" data-bind="attr: { href: source, title: title }" href=""><span class="go">Go to source</span></a>
                            </div>
                            
                            <div class="row">
                                <div class=" large-12 columns">
                                    <ul class="large-block-grid-2">
                                        <li>
                                            <ul>
                                                <li><strong>Author:</strong> <span data-bind="text: author"></span></li>

                                                <li><strong>Published:</strong> <span data-bind="text: pubdate"></span></li>

                                                <li><strong>More info:</strong> <span data-bind="text: description"></span></li>
                                            </ul>
                                        </li>
                                        <li><strong>Locations used</strong>
                                            <ul data-bind="foreach: links">
                                                <li><a data-bind="attr: { href: $data[1], title: $data[0] }, text: $data[0]" href="" target="_blank"></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>

            </div>
        </div>
    </div>